import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        testMap();
    }

    public static void testFilter() {

        List<String> stringList = Arrays.asList("abc", "123", "", "anna", "joe");

        List filtered = stringList.stream().filter(item -> !item.isEmpty()).collect(Collectors.toList());

        System.out.println(filtered);


    }

    public static void testMap() {

        Item item3 = new Item(8l, "ripple");
        Item item1 = new Item(2l, "zcash");
        Item item2 = new Item(3l, "dogecoin");

        List<Item> itemList = new ArrayList<>();
        itemList.add(item3);
        itemList.add(item2);
        itemList.add(item1);

        List<Item> filtered = itemList.stream().map(item -> {
            Item tem = new Item(item.getId() + 1000l, item.getName() + " new");
            return tem;
        }).collect(Collectors.toList());


        filtered.stream().forEach(item -> System.out.println(item.getId() + " " + item.getName()));
        System.out.println(filtered);

        Optional<Item> first = itemList.stream().filter(item -> item.getId() == 1l).findFirst();
        System.out.println("find first " + first.get().getName());



    }

    public static class Item {
        Long id;
        String name;

        public Item(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
